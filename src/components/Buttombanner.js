import  React , {Component} from 'react'


import {connect} from 'react-redux';


import {setToken,clearToken} from '../initializers/actions'

class Buttonbanner extends Component {
  constructor(props) {
    super(props)
      this.state = { busqueda: false}
  }



  componentWillMount(){
  
  }

  componentDidMount(){
        
  }

  render(){
      return(
        <div class="collections-bottom">
        <div class="container">
          <div class="collections-bottom-grids">
            <div class="collections-bottom-grid animated wow slideInLeft" data-wow-delay=".5s">
              <h3>45% Offer For <span>Women & Children's</span></h3>
            </div>
          </div>
          <div class="newsletter animated wow slideInUp" data-wow-delay=".5s">
            <h3>Newsletter</h3>
            <p>Join us now to get all news and special offers.</p>
            <form>
              <span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>
              <input type="email" value="Enter your email address" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Enter your email address';}" required=""/>
              <input type="submit" value="Join Us" />
            </form>
          </div>
        </div>
      </div>
      );
  }

}

const mapStateProps = (state)=>{
    return{
      token: state.token
    }
  }
  
  
  
  const mapDispatchToProps = {
   setToken,
   clearToken
  }
  
  export default connect(mapStateProps, mapDispatchToProps)(Buttonbanner);
  