import  React  from 'react'
import moment from 'moment'
import { Link } from 'react-router-dom';


export default function  Productcardcategory (props)  {
    return(
        <div className="col-md-4 margin-10">
            <div className="new-collections-grid1 products-right-grid1 " data-wow-delay=".5s">
                  <div className="new-collections-grid1-image">
                    <a href="single.html" className="product-image"><img src={props.product.img_primary} alt=" " className="img-responsive"/></a>
                    <div className="new-collections-grid1-image-pos products-right-grids-pos">
                      <Link to={'/product/'+props.product.id}>Ver mas</Link>
                    </div>
            
                  </div>
                  <h4><a href="single.html">{props.product.name}</a></h4>
                  <p>{props.product.description.substring(0,20)}.</p>
                  <div className="simpleCart_shelfItem products-right-grid1-add-cart">
                    {
                      props.product.price_distributor>0?
                      <p><i>{props.product.price}</i> <span className="item_price">{props.product.price_distributor}</span><a className="item_add" href="#">Añadir al carrito </a></p>:
                      <p><span className="item_price">{props.product.price}</span><a className="item_add" href="#">Añadir al carrito </a></p>
                    }
                  </div>
						</div>
        </div>
      );
  }


