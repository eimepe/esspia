import  React , {Component} from 'react'


import {connect} from 'react-redux';
import {Link} from 'react-router-dom';

import {setToken,clearToken} from '../initializers/actions'

class Sidebar extends Component {
  constructor(props) {
    super(props)
      this.state = { busqueda: false}
  }



  componentWillMount(){
  
  }

  componentDidMount(){
        
  }

  render(){
      return(
        <div className=" categories animated wow slideInUp" data-wow-delay=".5s">
        <h3>Categorias</h3>
        <ul className="cate">
        {
          this.props.categories.map((category)=>{
            return <li key={category.id}><Link to={"/category/"+category.id}>{category.name}</Link> </li>
          })
        }
          
          
        </ul>
      </div>
      );
  }

}

const mapStateProps = (state)=>{
    return{
      token: state.token
    }
  }
  
  
  
  const mapDispatchToProps = {
   setToken,
   clearToken
  }
  
  export default connect(mapStateProps, mapDispatchToProps)(Sidebar);
  