import  React , {Component} from 'react'


import {connect} from 'react-redux';


import {setToken,clearToken} from '../initializers/actions'
import Productcard from './Productcard';

class Newcollection extends Component {
  constructor(props) {
    super(props)
      this.state = { busqueda: false}
  }



  componentWillMount(){
  
  }

  componentDidMount(){
        
  }

  render(){
      return(
        <div class="new-collections">
        <div class="container">
          <h3 class="animated wow zoomIn" data-wow-delay=".5s">Promociones</h3>
       
          <div class="new-collections-grids row">
          {
            this.props.products.map((product)=>{
              return <Productcard product={product}/>
            })
          }
          
          
          </div>
        </div>
      </div>
      );
  }

}

const mapStateProps = (state)=>{
    return{
      token: state.token
    }
  }
  
  
  
  const mapDispatchToProps = {
   setToken,
   clearToken
  }
  
  export default connect(mapStateProps, mapDispatchToProps)(Newcollection);
  