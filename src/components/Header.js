import  React , {Component} from 'react'
import Modal from 'react-responsive-modal'
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import axios from 'axios'
import '../css/search.css'
import {setToken,clearToken, setQuantity, clearQuantity} from '../initializers/actions'
import Constants from '../initializers/Constants';

class Header extends Component {
  constructor(props) {
    super(props)
      this.state = { busqueda: false, open: false, categories: []}
  }

  onOpenModal = () => {
    this.setState({ open: true });
  };
 
  onCloseModal = () => {
    this.setState({ open: false });
  };

  findCategories(){
    axios({
      url: Constants.URL+'/subcategory?$limit=1000',
      method: 'GET',
      headers: {
        'Authorization': ''
      }
    }).then(res=>{
      //console.log(res);
      
     this.setState({categories: res.data.data})
      
      
    }).catch(console.log);
  }


  componentWillMount(){
  
  }

  componentDidMount(){
        this.findCategories();
        var form = document.getElementById("search-form");
console.log('where am i?');

document.getElementById("search-form").addEventListener("submit", function (event) {
    var searchText = document.querySelector('[name="search"]').value;
    console.log("submit '%s'", searchText);
    var searchText = document.querySelector('[name="search"]').value = '';
    event.preventDefault();
});

document.getElementById("search-text").addEventListener("click", function (event) {
    var searchText = document.querySelector('[name="search"]').value;
    if (searchText.trim().length > 0) {
        console.log(searchText);
    }
});
  }

  render(){
      return(
        <div className="header">
        <div className="container">
          <div className="header-grid">
            <div className="header-grid-left animated wow slideInLeft" data-wow-delay=".5s">
              <ul>
                <li><i className="glyphicon glyphicon-envelope" aria-hidden="true"></i><a href="mailto:info@esspia.com">info@esspia.com</a></li>
                <li><i className="glyphicon glyphicon-earphone" aria-hidden="true"></i>312 386 11 13</li>
                <li><i className="glyphicon glyphicon-log-in" aria-hidden="true"></i><a href="#" onClick={()=>this.setState({open: true})}>Entrar</a></li>
                <li><i className="glyphicon glyphicon-book" aria-hidden="true"></i><a href="register.html">Registrarse</a></li>
              </ul>
            </div>
            <div className="header-grid-right animated wow slideInRight" data-wow-delay=".5s">
              <ul className="social-icons">
                <li><a href="#" className="facebook"></a></li>
                <li><a href="#" className="twitter"></a></li>
                <li><a href="#" className="g"></a></li>
                <li><a href="#" className="instagram"></a></li>
              </ul>
            </div>
            <div className="clearfix"> </div>
          </div>
          <div className="logo-nav">
            <div className="logo-nav-left animated wow zoomIn" data-wow-delay=".5s">
              <h1><a href="index.html">Esspia <span>Security Systems</span></a></h1>
            </div>
            <div className="logo-nav-left1">
              <nav className="navbar navbar-default">
            
              <div className="navbar-header nav_2">
                <button type="button" className="navbar-toggle collapsed navbar-toggle1" data-toggle="collapse" data-target="#bs-megadropdown-tabs">
                  <span className="sr-only">Toggle navigation</span>
                  <span className="icon-bar"></span>
                  <span className="icon-bar"></span>
                  <span className="icon-bar"></span>
                </button>
              </div> 
              <div className="collapse navbar-collapse" id="bs-megadropdown-tabs">
                <ul className="nav navbar-nav">
                  <li className="active"><Link to="/" className="act">Inicio</Link></li>	
            
                  <li className="dropdown">
                    <a href="#" className="dropdown-toggle" data-toggle="dropdown">Products <b className="caret"></b></a>
                    <ul className="dropdown-menu multi-column columns-1">
                      <div className="row">
                      
                        <div className="col-sm-12">
                          <ul className="multi-column-dropdown">   
                            {this.state.categories.map((category)=>{
                              return <li key={category.id}><Link className="menu-desplegable" to={'/category/'+category.id}>{category.name}</Link></li>
                            })}
                            
                            
                          </ul>
                        </div>
                
                        <div className="clearfix"></div>
                      </div>
                    </ul>
                  </li>
               
                  <li><Link to="nosotros">Nosotros</Link></li>
                  <li><Link to="/contactenos">Contactenos</Link></li>
                  <li><a><div className="search-btn"> 
            <form id="search-form">  
            <input autocomplete="off" class="search-field" id="search-text" name="search" placeholder="Bucar" type="text" />  
            <input style={{display: 'none'}} type="submit" value="" /> 
            </form>
            </div> </a></li>
                </ul>
              </div>
              </nav>
            </div>
           
            <div className="header-right">
              <div className="cart box_1">
                <Link to="/cart">
                  <h3> <div className="total">
                    <span ></span> (<span>{this.props.quantity>0?this.props.quantity:0}</span> items)<span class="glyphicon glyphicon-shopping-cart"></span></div>
                    
                  </h3>
                </Link>
              
                <div className="clearfix"> </div>
              </div>	
            </div>
            <div className="clearfix"> </div>
          </div>
        </div>

        <Modal  open={this.state.open} onClose={this.onCloseModal} center>
        <div className="login">
            <div >
              <h3 className="animated wow zoomIn" data-wow-delay=".5s">Entrar</h3>
              <p className="est title-login animated wow zoomIn" data-wow-delay=".5s">Si tienes cuenta puedes iniciar sesion aqui.</p>
              <div className="login-form-grids animated wow slideInUp" data-wow-delay=".5s">
                <form>
                  <input type="email" placeholder="Correo electronico" required=" " />
                  <input type="password" placeholder="Contraseña" required=" " />
                  <div className="forgot">
                    <a href="#">Olvidaste tu contraseña?</a>
                  </div>
                  <input type="submit" value="Login"/>
                </form>
              </div>
              <h4 className="animated wow slideInUp" data-wow-delay=".5s">No tienes Cuenta?</h4>
              <p className="animated wow slideInUp" data-wow-delay=".5s"><a href="register.html">Registrate Aqui</a> Ir al <a href="/">Inicio<span className="glyphicon glyphicon-menu-right" aria-hidden="true"></span></a></p>
            </div>
          </div>
        </Modal>
      </div>
      );
  }

}

const mapStateProps = (state)=>{
    return{
      token: state.token,
      quantity: state.quantity
    }
  }
  
  
  
  const mapDispatchToProps = {
   setToken,
   clearToken,
   setQuantity,
   clearQuantity
  }
  
  export default connect(mapStateProps, mapDispatchToProps)(Header);
  