import  React , {Component} from 'react'
import moment from 'moment'
import { Link } from 'react-router-dom';


export default function  Productcard (props)  {
    return(
        <div className="col-md-3 margin-10">
                  <div className="new-collections-grid1 new-collections-grid1-image-width animated wow slideInUp" data-wow-delay=".5s">
                    <div className="new-collections-grid1-image">
                      <a href="single.html" className="product-image"><img src={props.product.img_primary}  alt=" " className="img-responsive" /></a>
                      <div className="new-collections-grid1-image-pos new-collections-grid1-image-pos1">
                      <Link to={'/product/'+props.product.id}>Ver mas</Link>
                      </div>
  
                      {
                        moment(props.product.createdAt).diff(new Date(), 'days')<=-30?<div className="new-one"><p>New</p></div>:''
                      }
                      
                    </div>
                    <h4><a href="single.html">{props.product.name}</a></h4>
                    <p>{props.product.description.substring(0,32)}.</p>
                    <div className="new-collections-grid1-left simpleCart_shelfItem">
                      <p><i>{props.product.price}</i> <span className="item_price">{props.product.price_distributor}</span><a className="item_add" href="#">Añadir </a></p>
                    </div>
                  </div>
              </div>
      );
  }


