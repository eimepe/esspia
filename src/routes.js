import React from 'react';
import {Route, Switch } from 'react-router-dom';

import App from './App';
import Home from './containers/Home';
import Category from './containers/Category';
import  Product from './containers/Product';
import Cart from './containers/Cart';
import Contact from './containers/Contact';
import About from './containers/About';



const AppRoutes = ()=>
<App>
  <Switch>
  <Route path='/category/:id' component={Category}/>
  <Route path='/product/:id' component={Product}/>
  <Route path='/cart' component={Cart}/>
  <Route path='/contactenos' component={Contact}/>
  <Route path='/nosotros' component={About}/>
    <Route path='/' component={Home}/>
    
  </Switch>
</App>


export default AppRoutes;
