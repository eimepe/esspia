import React, { Component } from 'react';
import {connect} from 'react-redux';
import {setToken,clearToken, setProduct, clearProduct, setCategories, clearCategories, setSale, clearSale, setQuantity, clearQuantity} from '../initializers/actions'
import axios from 'axios'

import ReactImageMagnify from 'react-image-magnify';
import Sliderrs from "react-slick";
import Header from '../components/Header'
import Banner from '../components/Banner'
import Destacados from '../components/Destacados';
import Newcollection from '../components/Newcollection';
import Timer from '../components/Timer';
import Buttombanner from '../components/Buttombanner';
import Loader from 'react-loader-spinner'
import Footer from '../components/Footer';
import Constants from '../initializers/Constants';
import Sidebar from '../components/Sidebar';
import '../css/react-slick.css';
import ReactSlick from '../components/ReactSlick'
import {ToastsContainer, ToastsStore} from 'react-toasts';
class Product extends Component {
  constructor(props) {
    super(props)
      this.state = { busqueda: false, loader: false, addproduct: false}

	}
	

	statusSale(){
		if(typeof this.props.sale.id  !== 'undefined') {
		fetch(Constants.URL+'/sales/'+this.props.sale.id, {
		 method: 'get',
		 headers: {
			 'Accept': 'application/json, text/plain, */*',
			 'Content-Type': 'application/json',
			 'authorization': this.props.token
		 }

		}).then(res=>res.json())
		 .then(res => {
			 if(res.code == 404){
				this.props.clearSale();
				this.props.clearQuantity();
			 }else{
				this.props.setQuantity(res.quantity);
				console.log(res.quantity);
			 }

				


		 });
	 }
	}


	statusStarts(){

		fetch(Constants.URL+'/valoracion?product='+this.props.match.params.id, {
		 method: 'get',
		 headers: {
			 'Accept': 'application/json, text/plain, */*',
			 'Content-Type': 'application/json',
			 'authorization': this.props.token
		 }

		}).then(res=>res.json())
		 .then(res => {
		 
		 this.setState({valoracion: res.valoracion});   


		 });
	 
	}


	setAdditem(product){
		fetch(Constants.URL+'/saleitems?sale_id='+this.props.sale.id+'&product_id='+product.id, {
		 method: 'get',
		 headers: {
			 'Accept': 'application/json, text/plain, */*',
			 'Content-Type': 'application/json',
			 'authorization': this.props.token
		 }

		}).then(res=>res.json())
		 .then(res => {

			if(res.total <= 0){
					var datas = {
					 sale_id: this.props.sale.id,
					 product_id: product.id,
					 description: product.description,
					 quantity: 1,
					 price_unitary: product.price_distributor >0 ? product.price_distributor : product.price,
					 price_total: product.price_distributor >0? product.price_distributor  : product.price,
					
						};

						fetch(Constants.URL+'/saleitems', {
						 method: 'post',
						 headers: {
							 'Accept': 'application/json, text/plain, */*',
							 'Content-Type': 'application/json',
							 'authorization': this.props.token
						 },
						 body: JSON.stringify(datas)
						}).then(res=>res.json())
						 .then(res => {

							console.log(res);
							this.statusSale();
							ToastsStore.success("Producto Agregado al carrito correctamente.");
							this.setState({addproduct: true, loader: false, respuestaaa: "Producto Agregado al carrito correctamente."});


						 });
			}else{
				console.log(res.data[0].quantity);
				 var datas = {

					 quantity: res.data[0].quantity+1,
					 price_total: product.price_distributor > 0? parseInt(res.data[0].price_total)+parseInt(product.price_distributor) : parseInt(res.data[0].price_total)+parseInt(product.price)
						};

						fetch(Constants.URL+'/saleitems/'+res.data[0].id, {
						 method: 'PATCH',
						 headers: {
							'Accept': 'application/json, text/plain, */*',
							 'Content-Type': 'application/json',
							 'authorization': this.props.token
						 },
						 body: JSON.stringify(datas)
						}).then(res=>res.json())
						 .then(res => {

							 console.log(res);
							 this.statusSale();
							 ToastsStore.success("Producto Agregado al carrito correctamente.");
							this.setState({addproduct: true, loader: false, respuestaaa: "Producto Agregado al carrito correctamente."});


						 });
			}


		 });

	}


			setVenta(product, ev){
				this.setState({loader: true, addproduct: false});
				console.log(this.props.sale);
				ev.preventDefault();
					if(typeof this.props.sale.id  == 'undefined') {

								var datas = {
							 clientid: this.props.token?1:"",
							 comment: "",
							 status_sale: 1,
							 type_sale: 1,
							 proveedor: product.userid

								};

								fetch(Constants.URL+'/sales', {
								 method: 'post',
								 headers: {
									 'Accept': 'application/json, text/plain, */*',
									 'Content-Type': 'application/json',
									 'authorization': this.props.token
								 },
								 body: JSON.stringify(datas)
								}).then(res=>res.json())
								 .then(res => {

									 
									 this.props.setSale(res);


								 });


						}
						var context = this;

						setTimeout(function(){
							context.setAdditem(product);
						}, 1000);
			}


	random(){
	 return Math.random().toString(36).substr(2); // Eliminar `0.`
		 }
	//end products


	show(){
		this.setState({show: true});
	}
 
	close(){
		this.setState({show: false});
	}


  findProducts(){
    axios({
      url: Constants.URL+'/products/'+this.props.match.params.id,
      method: 'GET',
      headers: {
        'Authorization': ''
      }
    }).then(res=>{
      //console.log(res);
      
      this.props.setProduct(res.data);
      
      
    }).catch(console.log);

	}
	
	findCategories(){
    axios({
      url: Constants.URL+'/subcategory?$limit=1000',
      method: 'GET',
      headers: {
        'Authorization': ''
      }
    }).then(res=>{
      //console.log(res);
      
      this.props.setCategories(res.data.data);
      
      
    }).catch(console.log);

  }

  componentWillMount(){
  
	}
	
	componentDidUpdate(prevProps) {
  
		// Typical usage (don't forget to compare props):
		if (this.props.match.params.id !== prevProps.match.params.id) {
			this.findProducts();
				this.findCategories();
			window.scrollTo(0, 0);
		}
	}

  componentDidMount(){
				this.findProducts();
				this.findCategories();
				this.statusSale();
				console.log(this.props);
				window.scrollTo(0, 0);
        
  }

  render() {
		var settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1
    };
    
    return ( 
        <div className="container-80 padding-40 white">
        <Header/>
      	<div class="breadcrumbs">
		<div class="container">
			<ol class="breadcrumb breadcrumb1 animated wow slideInLeft" data-wow-delay=".5s">
				<li><a href="/"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>Inicio</a></li>
				<li class="active">Producto</li>
			</ol>
		</div>
	</div>


  <div class="single">
		<div class="container">
			<div class="col-md-4 products-left">

					<Sidebar categories={this.props.categories}/>

			</div>
			<div class="col-md-8 single-right">
				<div class="col-md-5 single-right-left animated wow slideInUp" data-wow-delay=".5s">
					<div class="flexslider">
					<ReactSlick  {...{
                        rimProps: {
                            enlargedImagePortalId: 'portal',
                            enlargedImageContainerDimensions: {
                                width: '200%',
                                height: '100%'
                            }
												}
												
                    }}/>
          
						 
					</div>
			
					
		
				</div>
		
				<div  class=" col-md-7 single-right-left simpleCart_shelfItem animated wow slideInRight" data-wow-delay=".5s" >
				<div id="portal" className="portal"></div>
					<h3>{this.props.product.name}</h3>
					<h4><span class="item_price">$550</span> - $900</h4>
	
					<div class="description">
					{this.state.loader?<Loader 
         type="Oval"
         color="green"
         height="60"	
         width="60"
      />:''  }
					
						<h5><i>Descripcion</i></h5>
						<p>{this.props.product.description}.</p>
					</div>
		
					<ToastsContainer  store={ToastsStore}/>
					<div class="occasion-cart">
						<a class="item_add" href="#" onClick={this.setVenta.bind(this, this.props.product)}>Añadir al carrito  </a>
					</div>
					<div class="social">
						<div class="social-left">
							<p>Compartir en :</p>
						</div>
						<div class="social-right">
							<ul class="social-icons">
								<li><a href="#" class="facebook"></a></li>
								<li><a href="#" class="twitter"></a></li>
								<li><a href="#" class="g"></a></li>
								<li><a href="#" class="instagram"></a></li>
							</ul>
						</div>
						<div class="clearfix"> </div>
					</div>
				</div>
				<div class="clearfix"> </div>
				<div class="bootstrap-tab animated wow slideInUp" data-wow-delay=".5s">
					
				</div>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>

        <Footer/>
        </div> 
    );
  }
}

const mapStateProps = (state)=>{
  return{
    token: state.token,
		product: state.product,
		categories: state.categories,
		sale: state.sale
  }
}



const mapDispatchToProps = {
 setToken,
 clearToken,
 setProduct,
 clearProduct,
 setCategories,
 clearCategories,
 setSale,
 clearSale,
 setQuantity,
 clearQuantity
}

export default connect(mapStateProps, mapDispatchToProps)(Product);
