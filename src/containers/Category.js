import React, { Component } from 'react';
import {connect} from 'react-redux';
import {setToken,clearToken, setProducts, clearProducts, setCategories, clearCategories} from '../initializers/actions'
import axios from 'axios'

import ReactImageMagnify from 'react-image-magnify';
import Header from '../components/Header'
import Banner from '../components/Banner'
import Destacados from '../components/Destacados';
import Newcollection from '../components/Newcollection';
import Timer from '../components/Timer';
import Buttombanner from '../components/Buttombanner';
import Footer from '../components/Footer';
import Constants from '../initializers/Constants';
import Productcardcategory from '../components/Productcardcategory';
import Sidebar from '../components/Sidebar';


class Category extends Component {
  constructor(props) {
    super(props)
      this.state = { busqueda: false}

  }


  findProducts(){
    axios({
      url: Constants.URL+'/products?category='+this.props.match.params.id+'&$limit=8&promo=1',
      method: 'GET',
      headers: {
        'Authorization': ''
      }
    }).then(res=>{
      //console.log(res);
      
      this.props.setProducts(res.data.data);
      
      
    }).catch(console.log);

	}
	
	findCategories(){
    axios({
      url: Constants.URL+'/subcategory?$limit=1000',
      method: 'GET',
      headers: {
        'Authorization': ''
      }
    }).then(res=>{
      //console.log(res);
      
      this.props.setCategories(res.data.data);
      
      
    }).catch(console.log);

  }

  componentWillMount(){
  
	}
	
	componentDidUpdate(prevProps) {
  
		// Typical usage (don't forget to compare props):
		if (this.props.match.params.id !== prevProps.match.params.id) {
			this.findProducts();
			this.findCategories();
			window.scrollTo(0, 0);
		}
	}

  componentDidMount(){
				this.findProducts();
				this.findCategories();
        console.log(this.props);
        
  }

  render() {

    
    return ( 
		<div>
			<div className="container-80 padding-40 white">
				<Header/>
				<div className="breadcrumbs">
					<div className="container">
						<ol className="breadcrumb breadcrumb1 animated wow slideInLeft" data-wow-delay=".5s">
							<li><a href="index.html"><span className="glyphicon glyphicon-home" aria-hidden="true"></span>Inicio</a></li>
							<li className="active">Productos</li>
						</ol>
					</div>
				</div>
					<div className="products">
						<div className="container">
							<div className="col-md-4 products-left">
								<Sidebar categories={this.props.categories}/>
							</div>
							<div className="col-md-8 products-right">
								<div className="products-right-grids-bottom padding-0">
									{
										this.props.products.map((product)=>{
											return <Productcardcategory key={product.id} product={product}/>
										})
									}
								<div class="clearfix"></div>
								</div>	
								<nav className="numbering animated wow slideInRight" data-wow-delay=".5s">
									<ul className="pagination paging">
									<li>
										<a href="#" aria-label="Previous">
										<span aria-hidden="true">&laquo;</span>
										</a>
									</li>
									<li className="active"><a href="#">1<span className="sr-only">(current)</span></a></li>
									<li><a href="#">2</a></li>
									<li><a href="#">3</a></li>
									<li><a href="#">4</a></li>
									<li><a href="#">5</a></li>
									<li>
										<a href="#" aria-label="Next">
										<span aria-hidden="true">&raquo;</span>
										</a>
									</li>
									</ul>
								</nav>
							</div>
					
						</div>
					</div>
					
			</div>
			<Footer/>
    </div>   
    );
  }
}

const mapStateProps = (state)=>{
  return{
    token: state.token,
		products: state.products,
		categories: state.categories
  }
}



const mapDispatchToProps = {
 setToken,
 clearToken,
 setProducts,
 clearProducts,
 setCategories,
 clearCategories
}

export default connect(mapStateProps, mapDispatchToProps)(Category);
