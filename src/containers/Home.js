import React, { Component } from 'react';
import {connect} from 'react-redux';
import {setToken,clearToken, setProducts, clearProducts} from '../initializers/actions'
import axios from 'axios'


import Header from '../components/Header'
import Banner from '../components/Banner'
import Destacados from '../components/Destacados';
import Newcollection from '../components/Newcollection';
import Timer from '../components/Timer';
import Buttombanner from '../components/Buttombanner';
import Footer from '../components/Footer';
import Constants from '../initializers/Constants';
import WOW from 'wowjs';
//import $ from 'jquery'
//import  '../../public/js/jquery.wmuSlider'

class Home extends Component {
  constructor(props) {
    super(props)
      this.state = { busqueda: false}

  }


  findProducts(){
    axios({
      url: Constants.URL+'/products?$limit=8&promo=1',
      method: 'GET',
      headers: {
        'Authorization': ''
      }
    }).then(res=>{
      //console.log(res);
      
      this.props.setProducts(res.data.data);
      
      
    }).catch(console.log);

  }

  componentWillMount(){
  
  }

  componentDidMount(){
        this.findProducts();
        console.log(this.props);
        new WOW.WOW({
          live: false
      }).init();
     
       
        
  }

  render() {

    
    return ( 
        <div className="container-80 padding-40 white">
        <Header/>
        <Banner/>
        <Destacados/>
        <Newcollection products={this.props.products}/>
        <Timer/>
        <Buttombanner/>
        <Footer/>
        </div> 
    );
  }
}

const mapStateProps = (state)=>{
  return{
    token: state.token,
    products: state.products
  }
}



const mapDispatchToProps = {
 setToken,
 clearToken,
 setProducts,
 clearProducts
}

export default connect(mapStateProps, mapDispatchToProps)(Home);
