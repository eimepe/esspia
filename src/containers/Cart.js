import React, { Component } from 'react';
import {connect} from 'react-redux';
import {setToken,clearToken, setProduct, clearProduct, setCategories, clearCategories, setSale, clearSale, setQuantity, clearQuantity} from '../initializers/actions'
import axios from 'axios'

import ReactImageMagnify from 'react-image-magnify';
import Sliderrs from "react-slick";
import Header from '../components/Header'
import Banner from '../components/Banner'
import Destacados from '../components/Destacados';
import Newcollection from '../components/Newcollection';
import Timer from '../components/Timer';
import Buttombanner from '../components/Buttombanner';
import Loader from 'react-loader-spinner'
import Footer from '../components/Footer';
import Constants from '../initializers/Constants';
import Sidebar from '../components/Sidebar';
import '../css/react-slick.css';
import ReactSlick from '../components/ReactSlick'
import {ToastsContainer, ToastsStore} from 'react-toasts';
class Product extends Component {
  constructor(props) {
    super(props)
      this.state = { busqueda: false, loader: false, addproduct: false, products: []}

	}
	

	getUser(){

		fetch(Constants.URL+'/users/', {
		 method: 'get',
		 headers: {
			 'Accept': 'application/json, text/plain, */*',
			 'Content-Type': 'application/json',
			 'authorization': localStorage.auth
		 }

		}).then(res=>res.json())
		 .then(res => {
			 if(res.code == 401){
				localStorage.removeItem("auth");

			}else{
				this.setState({user: res.logeado});

			}




		 });

	}



getSale(){
	if(typeof this.props.sale.id !== 'undefined') {
	fetch(Constants.URL+'/sales/'+this.props.sale.id, {
	 method: 'get',
	 headers: {
		 'Accept': 'application/json, text/plain, */*',
		 'Content-Type': 'application/json',
		 'authorization': this.props.token
	 }

	}).then(res=>res.json())
	 .then(res => {
		 if(res.code == 404){
			this.props.clearSale();

		}else{
			this.getItemssale(res.id);
			this.props.setQuantity(res.quantity);



		}




	 });
 }
}




getItemssale(id){

	if(id !== 'undefined') {
	fetch(Constants.URL+'/saleitems?sale_id='+id, {
	 method: 'get',
	 headers: {
		 'Accept': 'application/json, text/plain, */*',
		 'Content-Type': 'application/json',
		 'authorization': this.props.token
	 }

	}).then(res=>res.json())
	 .then(res => {
		 if(res.code == 404){
			this.props.clearSale();

		}else{
				console.log(res.data);
				

		this.setState({products: res.data});

		}
	 });
 }
}

Restar(product, ev){

	this.setState({loader: true});
	if (product.quantity==1) {
		fetch(Constants.URL+'/saleitems/'+product.id, {
			method: 'DELETE',
			headers: {
				'Accept': 'application/json, text/plain, */*',
				'Content-Type': 'application/json',
				'authorization': this.props.token
			},
			body: JSON.stringify({
			 quantity: cantidad,
			 price_total: cantidad*product.price_unitary
				})
 
		 }).then(res=>res.json())
			.then(res => {
				if(res.code == 404){
				 this.props.clearSale();
 
			 }else{
					 console.log(res.data);
					 
 
					 this.getSale();
					 this.setState({loader: false});

					 this.getUser();
 
			 }
			});
	}else{
		var cantidad = product.quantity-1;
		fetch(Constants.URL+'/saleitems/'+product.id, {
		 method: 'PATCH',
		 headers: {
			 'Accept': 'application/json, text/plain, */*',
			 'Content-Type': 'application/json',
			 'authorization': this.props.token
		 },
		 body: JSON.stringify({
			quantity: cantidad,
			price_total: cantidad*product.price_unitary
			 })

		}).then(res=>res.json())
		 .then(res => {
			 if(res.code == 404){
				this.props.clearSale();

			}else{
					console.log(res.data);
					

					this.getSale();
					this.setState({loader: false});

					this.getUser();

			}
		 });
	}
	
	 
}


Sumar(product, ev){
	this.setState({loader: true});
	var cantidad = product.quantity+1;
		fetch(Constants.URL+'/saleitems/'+product.id, {
		 method: 'PATCH',
		 headers: {
			 'Accept': 'application/json, text/plain, */*',
			 'Content-Type': 'application/json',
			 'authorization': this.props.token
		 },
		 body: JSON.stringify({
			quantity: cantidad,
			price_total: cantidad*product.price_unitary
			 })

		}).then(res=>res.json())
		 .then(res => {
			 if(res.code == 404){
				this.props.clearSale();

			}else{
					console.log(res.data);
					
					this.setState({loader: false});
					this.getSale();


					this.getUser();

			}
		 });
	 
}

handleOptionChange(ev){
if (ev.target.value == 1) {
	console.log(ev.target.value);
	this.setState({pago: 'online'})

} else if(ev.target.value == 2){
	console.log(ev.target.value);
	this.setState({pago: 'entrega'})
}else{
	console.log(ev.target.value);
	this.setState({pago: ''})
}

}
getLogin(ev){
ev.preventDefault();

}

getRegister(ev){
ev.preventDefault();

}


componentDidMount(){
	this.getSale();


this.getUser();

}

  render() {
		var settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1
    };
    
    return ( 
        <div className="container-80 padding-40 white">
        <Header/>
      	<div class="breadcrumbs">
		<div class="container">
			<ol class="breadcrumb breadcrumb1 animated wow slideInLeft" data-wow-delay=".5s">
				<li><a href="/"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>Inicio</a></li>
				<li class="active">Carrito</li>
			</ol>
		</div>
	</div>


	<div class="checkout">
		<div class="container">
			<h3 class="animated wow slideInLeft" data-wow-delay=".5s">SU CARRITO DE COMPRAS CONTIENE: <span>{this.props.quantity} Productos</span></h3>
			<div class="checkout-right animated wow slideInUp" data-wow-delay=".5s">
				<table class="timetable_sub">
					<thead>
						<tr>
							<th>ID.</th>	
						
							<th>Cantidad</th>
							<th>Producto</th>
						
							<th>Precio</th>
							<th>Sub-Total</th>
							
						</tr>
					</thead>
					
					{this.state.products.map((product)=>{
						return 					<tr class="rem2">
						<td class="invert">2</td>
						
						<td class="invert">
							 <div class="quantity"> 
								<div class="quantity-select">                           
									<div class="entry value-minus">&nbsp;</div>
									<div class="entry value"><span>{product.quantity}</span></div>
									<div class="entry value-plus active">&nbsp;</div>
								</div>
							</div>
						</td>
						<td class="invert">{product.products.name}</td>
						
						<td class="invert">{product.price_unitary}</td>
						<td class="invert">{product.price_total}</td>
						
					</tr>
					})}


								
									
				</table>
			</div>
			<div class="checkout-left">	
			<a className="alert alert-success" href="/"><span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>Ir a pagar</a>
				<div class="checkout-right-basket animated wow slideInRight" data-wow-delay=".5s">
					<a href="/"><span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span>Continue Shopping</a>
					

				</div>
			
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>

        <Footer/>
        </div> 
    );
  }
}

const mapStateProps = (state)=>{
  return{
    token: state.token,
		product: state.product,
		categories: state.categories,
		sale: state.sale,
		quantity: state.quantity
  }
}



const mapDispatchToProps = {
 setToken,
 clearToken,
 setProduct,
 clearProduct,
 setCategories,
 clearCategories,
 setSale,
 clearSale,
 setQuantity,
 clearQuantity
}

export default connect(mapStateProps, mapDispatchToProps)(Product);
