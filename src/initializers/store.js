import { createStore, combineReducers, compose} from 'redux';
import reduxLocal from 'redux-localstorage';
function token(state='',action){
    switch (action.type) {
        case 'SET_TOKEN':
            return action.token;
        case 'CLEAR_TOKEN':
            return '';
        default:
            return state;
    }
    
}

function products(state=[],action){
    switch (action.type) {
        case 'SET_PRODUCTS':
            return action.products;
        case 'CLEAR_PRODUCTS':
            return [];
        default:
            return state;
    }
    
}


function categories(state=[],action){
    switch (action.type) {
        case 'SET_CATEGORIES':
            return action.categories;
        case 'CLEAR_CATEGORIES':
            return [];
        default:
            return state;
    }
    
}

function product(state={},action){
    switch (action.type) {
        case 'SET_PRODUCT':
            return action.product;
        case 'CLEAR_PRODUCT':
            return {};
        default:
            return state;
    }
    
}

function quantity(state=0,action){
    switch (action.type) {
        case 'SET_QUANTITY':
            return action.quantity;
        case 'CLEAR_QUANTITY':
            return 0;
        default:
            return state;
    }    
}


function sale(state={},action){
    switch (action.type) {
        case 'SET_SALE':
            return action.sale;
        case 'CLEAR_SALE':
            return {};
        default:
            return state;
    }    
}

let rootReducer = combineReducers({
    token: token,
    products: products,
    product: product,
    categories: categories,
    sale: sale,
    quantity: quantity

});

let mainEnhancer = compose(reduxLocal());

export default createStore(rootReducer, {}, mainEnhancer);

