export const setToken = (token)=>{
    return {
        type: 'SET_TOKEN',
        token: token
    }
}

export const clearToken = ()=>{
    return {
        type: 'CLEAR_TOKEN'
    }
}

export const setProducts = (products)=>{
    return {
        type: 'SET_PRODUCTS',
        products: products
    }
}

export const clearProducts = ()=>{
    return {
        type: 'CLEAR_PRODUCTS'
    }
}

export const setProduct = (product)=>{
    return {
        type: 'SET_PRODUCT',
        product: product
    }
}

export const clearProduct = ()=>{
    return {
        type: 'CLEAR_PRODUCT'
    }
}

export const setCategories = (categories)=>{
    return {
        type: 'SET_CATEGORIES',
        categories: categories
    }
}

export const clearCategories = ()=>{
    return {
        type: 'CLEAR_CATEGORIES'
    }
}

export const setSale = (sale)=>{
    return {
        type: 'SET_SALE',
        sale: sale
    }
}

export const clearSale = ()=>{
    return {
        type: 'CLEAR_SALE'
    }
}
export const setQuantity = (quantity)=>{
    return {
        type: 'SET_QUANTITY',
        quantity: quantity
    }
}

export const clearQuantity = ()=>{
    return {
        type: 'CLEAR_QUANTITY'
    }
}